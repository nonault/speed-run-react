import { useState } from "react";
import { useSearchParams } from "react-router-dom";

import React from 'react';
import "./Home.css";
import data from "../data.json";
import HorizontalList from './HorizontalList';
import Input from './Input';

function Home() {
  
  const [paramDeRecherche, setSearchParams] = useSearchParams();
  const [value, setValue] = useState(paramDeRecherche.get("q"));
  const onChange = (event) => {
    setValue(event.target.value);
    setSearchParams(event.target.value ? { q: event.target.value } : {});
  };
  const listeFilms = data.movies.filter((movie) =>
    movie.title.match(new RegExp(value, "i"))
  );
    return (
      <div>
        <Input value={value} onChange={onChange} />
        <HorizontalList  data={listeFilms} />
      </div>
    );
  }
  
  export default Home;

  