import { Navigate, useParams } from "react-router-dom";

import "./Movie.css";

import data from "../data.json";
import Rating from "./Rating";

function affichageNote(value){
  if (value<0){
    return -1;
  }
  return value;
}

function Movie() {
  
  const { id } = useParams();

  const filmActuel = data.movies.find((movie) => movie.id.toString() === id);
  if (!filmActuel) return <Navigate to="/" replace={true} />;
  return (
    <div >
      <div>
        <h1 >{filmActuel.title}</h1>
        <img
          src={`https://image.tmdb.org/t/p/w92${filmActuel.poster_path}`}
          alt={filmActuel.title}
        />
        <div>
          <p>
            sorti le{" "}
            {new Intl.DateTimeFormat("fr", { dateStyle: "long" }).format(new Date(filmActuel.release_date))}
          </p>
        </div>
        <div className="Note">
          <h2>Note : </h2>
          <Rating value={affichageNote(filmActuel.vote_average)} /> 
        </div>
      </div>
      
    </div>
  );
}

export default Movie;
