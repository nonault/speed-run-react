import React from 'react';
import "./Card.css";

function Card({poster_path}) {
  const imageURL = `https://image.tmdb.org/t/p/w92${poster_path}`;
    return (
      <div className="Card">
        <img src={imageURL}/>
      </div>
    );
  }
  
  export default Card;