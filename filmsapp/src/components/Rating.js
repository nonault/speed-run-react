import React from 'react';
import "./Rating.css";

function Rating({ value }) {
  const stars = Math.trunc(value);
    return (
      <div className='Note'>
        <div className='Etoile'>
          {stars}
        </div>
      </div>
    );
  }
  
  export default Rating;