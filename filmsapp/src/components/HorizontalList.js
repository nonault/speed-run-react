import React from 'react';
import { Link } from "react-router-dom";
import "./HorizontalList.css";

import Card from "./Card";

function HorizontalList({ data }) {
  return (
    <ul className='List'>
      {data.map((entry) => (
        <li key={entry.id}>
          <Link to={`/movies/${entry.id}`}>
            <Card {...entry} />
          </Link>
        </li>
      ))}
    </ul>
  );
}
  
export default HorizontalList;